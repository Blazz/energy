package com.skyline.energy.annotation.jdbc;

/**
 * 水平切分时的键
 * 
 * 
 * <pre>
 * 例1：
 * 基本SQL查询：
 * <code>@Query("select * from album where id=:id and userId=:userId limit 1")</code>
 * <code>@MapperBy(AlbumMapper.class)</code>
 * <code>@Unique</code>
 * <code>@ShardBy("userId")
 * public Album queryAlbumDetailById(@Param("id") long albumId, @Param("userId") long userId);</code>
 * 最后查询时就会按照userId的值进行路由，找到对应的DataSource
 * </pre>
 * 
 * @author wuqh
 * 
 */
public @interface ShardBy {
	/**
	 * 键值对应的参数
	 * 
	 * @return
	 */
	String value();
}
