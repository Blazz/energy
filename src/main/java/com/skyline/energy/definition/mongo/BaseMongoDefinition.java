package com.skyline.energy.definition.mongo;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;

import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;

import com.skyline.common.reflection.TypeUtils;
import com.skyline.energy.annotation.mongo.MongoCollection;
import com.skyline.energy.definition.AbstractDefinition;
import com.skyline.energy.exception.DaoGenerateException;
import com.skyline.energy.expression.ExpressionParser;
import com.skyline.energy.expression.ParsedExpression;
import com.skyline.energy.expression.ParserFactory;
import com.skyline.energy.expression.ParserFactory.ExpressionType;
import com.skyline.energy.utils.ExpressionUtils;

/**
 * 通过对配置了@MongoFind等方法的解析， 产生需要在执行Mongo操作时必要用到的参数。
 * BaseMongoDefinition中配置了一些共有的或者较为基本的参数。
 * 
 * @author wuqh
 * 
 */
public abstract class BaseMongoDefinition extends AbstractDefinition {
	/**
	 * 需要绑定数据的Shell
	 */
	private String shellWithToken;

	/**
	 * <code>@MongoCollection</code>参数放在args的位置
	 * 
	 */
	private int collectionIndex = -1;

	/**
	 * <code>@MongoCollection</code>中value的值
	 */
	String globalCollectionName;

	BaseMongoDefinition(Method method) throws DaoGenerateException {
		super(method);
	}

	@Override
	protected void parseInternal(Method method, Map<String, Integer> paramIndexes,
			Map<String, Integer> batchParamIndexes) throws DaoGenerateException {
		MongoCollection collection = method.getAnnotation(MongoCollection.class);
		if (collection != null) {
			globalCollectionName = collection.value();
		}

		Annotation[][] annotations = method.getParameterAnnotations();
		Class<?>[] paramTypes = method.getParameterTypes();
		for (int index = 0; index < annotations.length; index++) {

			for (Annotation annotation : annotations[index]) {
				Class<? extends Annotation> annotationType = annotation.annotationType();
				if (MongoCollection.class.equals(annotationType)) {
					if (TypeUtils.isTypeString(paramTypes[index])) {
						collectionIndex = index;
					}
				}
			}
		}
	}

	@Override
	protected void checkBeforeParse(Method method) throws DaoGenerateException {
	}

	@Override
	protected void checkAfterParse(Method method) throws DaoGenerateException {
		if (collectionIndex == -1 && StringUtils.isEmpty(globalCollectionName)) {
			throw new DaoGenerateException("方法[" + method + "]配置错误：没有配置@MongoCollection注解");
		}
	}

	@Override
	protected ParsedExpression parseExpression(Method method) {
		ExpressionParser parser = ParserFactory.createExpressionParser(ExpressionType.MONGO_SHELL);

		ParsedExpression parsedShell = parser.parse(getSourceShell(method));
		shellWithToken = ExpressionUtils.getSql(parsedShell);

		return parsedShell;
	}

	/**
	 * 获取方法中配置的原始Shell
	 * 
	 * @param method
	 * @return
	 */
	protected abstract String getSourceShell(Method method);

	/**
	 * 检查加了修改更新类注解的方法的返回值类型:必须返回void或者boolean，如果不是将抛出DaoGenerateException
	 * 
	 * @param method
	 * @param annotationName
	 * @throws DaoGenerateException
	 */
	void checkUpsetReturnType(Method method, String annotationName) throws DaoGenerateException {
		Class<?> returnType = method.getReturnType();

		if (TypeUtils.isTypePrimitive(returnType)) {
			returnType = ClassUtils.primitiveToWrapper(returnType);
		}
		if (TypeUtils.isTypeVoid(returnType) || Boolean.class.equals(returnType)) {
			return;
		}

		throw new DaoGenerateException("方法[" + method + "]配置错误：返回值非void,boolean,java.lang.Boolean类型，不能@"
				+ annotationName + "注解");

	}

	/**
	 * 获取需要操作collectionName
	 * 
	 * @param args
	 * @return
	 */
	public String getCollectionName(Object[] args) {
		if (collectionIndex == -1) {
			return globalCollectionName;
		}

		Object arg = args[collectionIndex];
		if (arg == null) {
			return globalCollectionName;
		} else {
			return arg.toString();
		}

	}

	public String getShellWithToken() {
		return shellWithToken;
	}

	public ParsedExpression getParsedShell() {
		return getParsedExpression();
	}

}
