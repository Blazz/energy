package com.skyline.energy.definition.jdbc;

import java.lang.reflect.Method;

import org.apache.commons.lang.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skyline.common.reflection.TypeUtils;
import com.skyline.energy.annotation.ReturnId;
import com.skyline.energy.annotation.jdbc.BatchUpdate;
import com.skyline.energy.definition.BatchDefinition;
import com.skyline.energy.exception.DaoGenerateException;

/**
 * 通过对配置了@BatchUpdate的方法的解析，产生需要在执行JDBC操作时必要用到的参数。
 * 
 * @author wuqh
 * 
 */
public class JdbcBatchUpdateDefinition extends BaseJdbcDefinition implements BatchDefinition {
	private static final Logger LOGGER = LoggerFactory.getLogger(JdbcBatchUpdateDefinition.class);
	private boolean isReturnId = false;
	private boolean isReturnList = false;
	private Class<?> returnComponentType;

	public boolean isReturnId() {
		return isReturnId;
	}

	public boolean isReturnList() {
		return isReturnList;
	}

	public Class<?> getReturnComponentType() {
		return returnComponentType;
	}

	public JdbcBatchUpdateDefinition(Method method) throws DaoGenerateException {
		super(method);
	}

	protected String getSourceSql(Method method) {
		// 解析批量修改的SQL语句
		BatchUpdate update = method.getAnnotation(BatchUpdate.class);
		return update.value();
	}

	@Override
	protected void checkBeforeParse(Method method) throws DaoGenerateException {
		super.checkBeforeParse(method);
		checkBatchReturnType(method);
		parseReturnInfo(method);
	}

	private void parseReturnInfo(Method method) {
		isReturnId = (method.getAnnotation(ReturnId.class) != null);
		if (isReturnId) {
			Class<?> returnType = method.getReturnType();

			if (TypeUtils.isTypeList(returnType)) {
				isReturnList = true;
			} else if (TypeUtils.isTypeArray(returnType)) {
				returnComponentType = returnType.getComponentType();
				isReturnList = false;
			}
		}
	}

	/**
	 * 检查加了@BatchUpdate注解的方法的返回值类型。规则： 如果有@ReturnId就必须是Number的父类或者基本类型的数组或者List
	 * 如果没有@ReturnId就必须返回void或者int[]
	 * 
	 * @param method
	 * @throws DaoGenerateException
	 */
	private void checkBatchReturnType(Method method) throws DaoGenerateException {
		Class<?> returnType = method.getReturnType();
		if (returnType == null || void.class.equals(returnType)) {
			return;
		}

		boolean isReturnId = (method.getAnnotation(ReturnId.class) != null);
		if (isReturnId) {
			checkReturnIdType(method, returnType);
		} else if (!int.class.equals(returnType.getComponentType())) {
			throw new DaoGenerateException("方法[" + method + "]配置错误：配置了@BatchUpdate注解的方法只能返回int[]；或者请增加@ReturnId注解");
		}
	}

	private void checkReturnIdType(Method method, Class<?> returnType) throws DaoGenerateException {
		if (!TypeUtils.isTypeArray(returnType) && !TypeUtils.isTypeList(returnType)) {
			throw new DaoGenerateException("方法[" + method + "]配置错误：配置了@ReturnId注解的方法只能返回数组或者List<? extends Number>类型对象");
		}
		if (TypeUtils.isTypeArray(returnType) && !ClassUtils.isAssignable(returnComponentType, Number.class, true)) {
			throw new DaoGenerateException("方法[" + method + "]配置错误：返回类型只能是基本类型数组或者java.lang.Number类型数组");
		}
	}

	@Override
	protected void logBindInfo(Method method) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("绑定" + getDescription() + "到方法[" + method + "]成功");
		}
	}

	private String getDescription() {
		String desc = "@BatchUpdate(" + this.getParsedSql().getOriginalExpression() + ")";

		if (this.isReturnId()) {
			desc = desc + ",@ReturnId()";
		}

		return desc;
	}

}
