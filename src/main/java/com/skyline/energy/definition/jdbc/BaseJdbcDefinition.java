package com.skyline.energy.definition.jdbc;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;

import com.skyline.energy.annotation.jdbc.ShardBy;
import com.skyline.energy.definition.AbstractDefinition;
import com.skyline.energy.exception.DaoGenerateException;
import com.skyline.energy.expression.ExpressionParser;
import com.skyline.energy.expression.ParsedExpression;
import com.skyline.energy.expression.ParserFactory;
import com.skyline.energy.expression.ParserFactory.ExpressionType;
import com.skyline.energy.utils.ArgumentUtils;
import com.skyline.energy.utils.DefinitionParseUtils;
import com.skyline.energy.utils.ExpressionUtils;
import com.skyline.energy.utils.SqlFormatter;

/**
 * 通过对配置了@Query，@Update，@BatchUpdate的方法的解析， 产生需要在执行JDBC操作时必要用到的参数。
 * BaseJdbcDefinition中配置了一些共有的或者较为基本的参数。
 * 
 * @author wuqh
 * 
 */
public abstract class BaseJdbcDefinition extends AbstractDefinition {

	BaseJdbcDefinition(Method method) throws DaoGenerateException {
		super(method);
	}

	/**
	 * 需要创建PreparedStatement的SQL
	 */
	private String preparedSql;

	private int shardKeyIndex = -1;

	private Method shardKeyGetter;

	private String shardBy;

	/**
	 * 获取解析后的SQL表达式
	 * 
	 * @return
	 */
	public ParsedExpression getParsedSql() {
		return getParsedExpression();
	}

	@Override
	protected ParsedExpression parseExpression(Method method) {
		ExpressionParser parser = ParserFactory.createExpressionParser(ExpressionType.SQL);

		ParsedExpression parsedSql = parser.parse(getSourceSql(method));
		preparedSql = ExpressionUtils.getSql(parsedSql);

		return parsedSql;
	}

	/**
	 * 获取方法中配置的原始SQL
	 * 
	 * @param method
	 * @return
	 */
	protected abstract String getSourceSql(Method method);

	@Override
	protected void checkBeforeParse(Method method) throws DaoGenerateException {
	}

	@Override
	protected void parseInternal(Method method, Map<String, Integer> paramIndexes,
			Map<String, Integer> batchParamIndexes) throws DaoGenerateException {

		parseShardBy(method, paramIndexes, batchParamIndexes);
	}

	@Override
	protected void checkAfterParse(Method method) throws DaoGenerateException {
	}

	private void parseShardBy(Method method, Map<String, Integer> paramIndexes, Map<String, Integer> batchParamIndexes)
			throws DaoGenerateException {
		ShardBy shardByAnnotation = method.getAnnotation(ShardBy.class);
		if (shardByAnnotation == null) {
			return;
		}

		shardBy = shardByAnnotation.value();

		if (batchParamIndexes.containsKey(shardBy)) {
			throw new DaoGenerateException("@ShardBy中的参数不能为@BatchParam");
		}

		List<String> parameterNames = new ArrayList<String>(1);
		parameterNames.add(shardBy);

		Class<?>[] paramTypes = method.getParameterTypes();

		Object[] gettersAndIndexes = DefinitionParseUtils
				.getGettersAndIndexes(parameterNames, paramIndexes, paramTypes);

		shardKeyGetter = ((Method[]) gettersAndIndexes[0])[0];
		shardKeyIndex = ((Integer[]) gettersAndIndexes[1])[0];
	}

	/**
	 * 替换SQL中的通用表名，获取实际用于执行的PreparedSQL
	 * 
	 * @param args
	 * @return
	 */
	public String getActualSql(Object[] args) {
		String[] tableNames = null;
		String sql;

		if (genericIndexes != null) {
			for (Integer index : genericIndexes) {
				if (index != null) {
					Object arg = args[index];
					if (arg != null) {
						tableNames = (String[]) ArrayUtils.add(tableNames, arg.toString());
					}
				}
			}
		}

		if (tableNames != null) {
			sql = SqlFormatter.formatSql(preparedSql, tableNames);
		} else {
			sql = preparedSql;
		}

		return sql;
	}

	public String getShardKey(Object[] args) {
		Object shardKey = ArgumentUtils.fetchValue(shardKeyGetter, shardKeyIndex, args, shardBy);
		if (shardKey == null) {
			return null;
		} else {
			return shardKey.toString();
		}
	}

}
