package com.skyline.energy.jdbc.impl.queued;

import com.skyline.common.concurrent.queue.Task;
import com.skyline.common.concurrent.queue.TaskParameters;
import com.skyline.common.concurrent.queue.TaskResult;
import com.skyline.common.utils.Assert;
import com.skyline.energy.jdbc.impl.PreparedStatementCallback;
import com.skyline.energy.jdbc.impl.SimpleJdbcDataAccessor;

/**
 * JDBC查询任务
 * 
 * @author wuqh
 * @see Task
 */
public class JdbcQueryTask extends Task {
	private final SimpleJdbcDataAccessor dataAccessor;

	public JdbcQueryTask(SimpleJdbcDataAccessor dataAccessor) {
		Assert.isNull(dataAccessor, "DataAccessor实例不能为空");
		this.dataAccessor = dataAccessor;
	}

	@SuppressWarnings("unchecked")
	public <T> TaskResult<T> process(TaskParameters parameters) {
		JdbcQueryTaskParameters taskParameters = (JdbcQueryTaskParameters) parameters;

		String sql = taskParameters.getSql();
		int fetchSize = taskParameters.getFetchSize();
		boolean returnKeys = taskParameters.isReturnKeys();
		PreparedStatementCallback<T> action = (PreparedStatementCallback<T>) taskParameters.getAction();
		Object[] args = taskParameters.getArgs();

		T result = dataAccessor.execute(sql, fetchSize, returnKeys, action, args);

		TaskResult<T> taskResult = new TaskResult<T>();
		taskResult.setResult(result);
		taskResult.setDone(true);

		return taskResult;
	}

}
