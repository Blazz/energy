package com.skyline.energy.jdbc.impl;

public enum SqlType {
	QUERY, UPDATE, INSERT
}
