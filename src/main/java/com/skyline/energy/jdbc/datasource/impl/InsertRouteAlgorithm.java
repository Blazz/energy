package com.skyline.energy.jdbc.datasource.impl;

import java.util.List;
import java.util.Random;

import javax.sql.DataSource;

import com.skyline.energy.jdbc.datasource.RouteAlgorithm;

public class InsertRouteAlgorithm implements RouteAlgorithm {
	private Random random = new Random();

	@Override
	public DataSource route(String key, List<? extends DataSource> nodes) {
		return randomGet(nodes);
	}

	protected DataSource randomGet(List<? extends DataSource> dataSources) {
		if (dataSources.size() == 0) {
			return null;
		}
		int index = random.nextInt(dataSources.size()); // 0.. size
		return dataSources.get(index);
	}
}
