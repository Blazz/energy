package com.skyline.energy.jdbc.datasource.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;

import com.skyline.energy.jdbc.datasource.NodeDataSource;
import com.skyline.energy.jdbc.datasource.RouteAlgorithm;

public class NormalRouteAlgorithm implements RouteAlgorithm {

	@Override
	public DataSource route(String key, List<? extends DataSource> nodes) {
		if (nodes.isEmpty()) {
			return null;
		}

		if (StringUtils.isBlank(key)) {
			return null;
		}

		long keyNumber = Long.parseLong(key);
		for (DataSource node : nodes) {
			NodeDataSource dataSource = (NodeDataSource) node;
			if (keyNumber >= dataSource.getStartNumber()) {
				return node;
			}
		}

		return null;
	}

}
