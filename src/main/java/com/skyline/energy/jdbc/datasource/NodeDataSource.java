package com.skyline.energy.jdbc.datasource;

import javax.sql.DataSource;

public class NodeDataSource extends AbstractDistributeDataSource {
	private DataSource dataSource;
	private boolean insertable;
	private long startNumber;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	protected DataSource getDataSource() {
		return dataSource;
	}

	public void setInsertable(boolean insertable) {
		this.insertable = insertable;
	}

	public boolean isInsertable() {
		return insertable;
	}

	public long getStartNumber() {
		return startNumber;
	}

	public void setStartNumber(long startNumber) {
		this.startNumber = startNumber;
	}

}
