package com.skyline.energy.jdbc.datasource;

import java.util.List;

import javax.sql.DataSource;

public interface RouteAlgorithm {
	DataSource route(String key, List<? extends DataSource> nodes);
}
