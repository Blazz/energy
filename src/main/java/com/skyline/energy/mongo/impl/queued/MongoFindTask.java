package com.skyline.energy.mongo.impl.queued;

import java.util.List;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.skyline.common.concurrent.queue.Task;
import com.skyline.common.concurrent.queue.TaskParameters;
import com.skyline.common.concurrent.queue.TaskResult;
import com.skyline.common.utils.Assert;
import com.skyline.energy.mongo.BeanMapper;
import com.skyline.energy.mongo.impl.SimpleMongoDataAccessor;

/**
 * Mongo查询任务
 * 
 * @author wuqh
 * @see Task
 */
public class MongoFindTask extends Task {
	private final SimpleMongoDataAccessor dataAccessor;

	public MongoFindTask(SimpleMongoDataAccessor dataAccessor) {
		Assert.isNull(dataAccessor, "DataAccessor实例不能为空");
		this.dataAccessor = dataAccessor;
	}

	@SuppressWarnings("unchecked")
	public <T> TaskResult<T> process(TaskParameters parameters) {
		MongoFindTaskParameters taskParameters = (MongoFindTaskParameters) parameters;

		DBCollection collection = taskParameters.getCollection();
		DBObject queryDbObject = taskParameters.getQueryDbObject();
		BeanMapper<?> mapper = taskParameters.getMapper();
		int skip = taskParameters.getSkip();
		int limit = taskParameters.getLimit();
		DBObject sort = taskParameters.getSort();
		int batchSize = taskParameters.getBatchSize();

		List<?> result = dataAccessor.findInternal(collection, queryDbObject, mapper, skip, limit, sort, batchSize);

		TaskResult<T> taskResult = new TaskResult<T>();
		taskResult.setResult((T) result);
		taskResult.setDone(true);

		return taskResult;
	}

}
