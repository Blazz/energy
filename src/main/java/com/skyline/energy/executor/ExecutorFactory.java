package com.skyline.energy.executor;

import java.lang.reflect.Method;

import com.skyline.common.cache.CacheManager;
import com.skyline.energy.annotation.jdbc.BatchUpdate;
import com.skyline.energy.annotation.jdbc.Query;
import com.skyline.energy.annotation.jdbc.Update;
import com.skyline.energy.annotation.mongo.MongoBatchInsert;
import com.skyline.energy.annotation.mongo.MongoCount;
import com.skyline.energy.annotation.mongo.MongoFind;
import com.skyline.energy.annotation.mongo.MongoInsert;
import com.skyline.energy.annotation.mongo.MongoRemove;
import com.skyline.energy.annotation.mongo.MongoUpdate;
import com.skyline.energy.exception.DaoGenerateException;
import com.skyline.energy.executor.cache.CacheExecutor;
import com.skyline.energy.executor.jdbc.AbstractJdbcExecutor;
import com.skyline.energy.executor.jdbc.JdbcBatchUpdateExecutor;
import com.skyline.energy.executor.jdbc.JdbcQueryExecutor;
import com.skyline.energy.executor.jdbc.JdbcUpdateExecutor;
import com.skyline.energy.executor.mongo.AbstractMongoExecutor;
import com.skyline.energy.executor.mongo.MongoBatchInsertExecutor;
import com.skyline.energy.executor.mongo.MongoCountExecutor;
import com.skyline.energy.executor.mongo.MongoFindExecutor;
import com.skyline.energy.executor.mongo.MongoInsertExecutor;
import com.skyline.energy.executor.mongo.MongoRemoveExecutor;
import com.skyline.energy.executor.mongo.MongoUpdateExecutor;
import com.skyline.energy.jdbc.JdbcDataAccessor;
import com.skyline.energy.mongo.MongoDataAccessor;

/**
 * 创建DataAccessExecutor的工厂类，
 * 用于创建JDBC的DataAccessExecutor和MongoDB的DataAccessExecutor
 * 
 * @author wuqh
 * @see DataAccessExecutor
 */
public final class ExecutorFactory {
	private ExecutorFactory() {
	}

	/**
	 * 创建JDBC的DataAccessExecutor
	 * 
	 * @param cacheManager
	 * @param dataAccessor
	 * @param method
	 * @return
	 * @throws DaoGenerateException
	 */
	public static DataAccessExecutor createJdbcExecutor(CacheManager cacheManager, JdbcDataAccessor dataAccessor,
			Method method) throws DaoGenerateException {
		DataAccessExecutor executor;

		CacheExecutor cacheExecutor = createCacheExecutor(cacheManager, method);
		AbstractJdbcExecutor jdbcExecutor = createJdbcExecutor(dataAccessor, method);

		if (jdbcExecutor == null) {
			cacheExecutor.setDataAccessExecutor(new MethodExecutor(method));
		} else {
			cacheExecutor.setDataAccessExecutor(jdbcExecutor);
		}

		executor = cacheExecutor;

		return executor;

	}

	/**
	 * 创建MongoDB的DataAccessExecutor
	 * 
	 * @param cacheManager
	 * @param dataAccessor
	 * @param method
	 * @return
	 * @throws DaoGenerateException
	 */
	public static DataAccessExecutor createMongoExecutor(CacheManager cacheManager, MongoDataAccessor dataAccessor,
			Method method) throws DaoGenerateException {
		DataAccessExecutor executor;
		CacheExecutor cacheExecutor = createCacheExecutor(cacheManager, method);
		AbstractMongoExecutor mongoExecutor = createMongoExecutor(dataAccessor, method);

		if (mongoExecutor == null) {
			cacheExecutor.setDataAccessExecutor(new MethodExecutor(method));
		} else {
			cacheExecutor.setDataAccessExecutor(mongoExecutor);
		}

		executor = cacheExecutor;

		return executor;

	}

	private static AbstractMongoExecutor createMongoExecutor(MongoDataAccessor dataAccessor, Method method)
			throws DaoGenerateException {
		AbstractMongoExecutor executor = null;

		if (method.getAnnotation(MongoFind.class) != null) {
			executor = new MongoFindExecutor(dataAccessor, method);
		} else if (method.getAnnotation(MongoUpdate.class) != null) {
			executor = new MongoUpdateExecutor(dataAccessor, method);
		} else if (method.getAnnotation(MongoInsert.class) != null) {
			executor = new MongoInsertExecutor(dataAccessor, method);
		} else if (method.getAnnotation(MongoRemove.class) != null) {
			executor = new MongoRemoveExecutor(dataAccessor, method);
		} else if (method.getAnnotation(MongoCount.class) != null) {
			executor = new MongoCountExecutor(dataAccessor, method);
		} else if (method.getAnnotation(MongoBatchInsert.class) != null) {
			executor = new MongoBatchInsertExecutor(dataAccessor, method);
		}

		return executor;
	}

	private static CacheExecutor createCacheExecutor(CacheManager cacheManager, Method method)
			throws DaoGenerateException {
		CacheExecutor executor = new CacheExecutor(cacheManager, method);

		return executor;
	}

	private static AbstractJdbcExecutor createJdbcExecutor(JdbcDataAccessor dataAccessor, Method method)
			throws DaoGenerateException {
		AbstractJdbcExecutor executor = null;
		if (method.getAnnotation(Query.class) != null) {
			executor = new JdbcQueryExecutor(dataAccessor, method);
		} else if (method.getAnnotation(Update.class) != null) {
			executor = new JdbcUpdateExecutor(dataAccessor, method);
		} else if (method.getAnnotation(BatchUpdate.class) != null) {
			executor = new JdbcBatchUpdateExecutor(dataAccessor, method);
		}

		return executor;
	}

}
