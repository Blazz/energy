package com.skyline.energy.executor;

import java.lang.reflect.Method;

import com.skyline.common.reflection.MethodUtils;

/**
 * DataAccessExecutor的默认调用实现
 * 
 * @author wuqh
 * 
 */
public class MethodExecutor implements DataAccessExecutor {
	private final Method method;

	public MethodExecutor(Method method) {
		this.method = method;
	}

	@Override
	public Object execute(Object obj, Object[] args) {
		return MethodUtils.invokeMethod(obj, method, args);
	}

}
