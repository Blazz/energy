package com.skyline.energy.executor.jdbc;

import java.lang.reflect.Method;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skyline.energy.definition.jdbc.JdbcUpdateDefinition;
import com.skyline.energy.exception.DaoGenerateException;
import com.skyline.energy.jdbc.JdbcDataAccessor;
import com.skyline.energy.jdbc.KeyHolder;
import com.skyline.energy.jdbc.impl.SqlThreadLocal;
import com.skyline.energy.utils.ArgumentUtils;

/**
 * JDBC的增删改操作调用方法
 * 
 * @author wuqh
 * 
 */
public class JdbcUpdateExecutor extends AbstractJdbcExecutor {
	private static final Logger LOGGER = LoggerFactory.getLogger(JdbcUpdateExecutor.class);
	private JdbcUpdateDefinition definition;

	public JdbcUpdateExecutor(JdbcDataAccessor dataAccessor, Method method) throws DaoGenerateException {
		super(dataAccessor, method);
	}

	@Override
	public Object execute(Object obj, Object[] args) {
		Method[] getterMethods = definition.getGetterMethods();
		Integer[] parameterIndexes = definition.getParameterIndexes();
		boolean isReturnId = definition.isReturnId();
		// 获取实际用于执行的preparedSQL
		String actualSql = definition.getActualSql(args);
		List<String> parameterNames = definition.getParsedSql().getParameterNames();
		Object[] paramArray = ArgumentUtils.fetchValues(getterMethods, parameterIndexes, args, parameterNames);

		KeyHolder keyHolder = null;
		if (isReturnId) {
			keyHolder = dataAccessor.getKeyHolder();
		}

		SqlThreadLocal.set(actualSql, definition.getShardKey(args));

		LOGGER.info("更新操作执行SQL[" + actualSql + "]");
		int rows = dataAccessor.update(actualSql, keyHolder, paramArray);

		if (keyHolder != null) {
			return keyHolder.getKey();
		} else {
			return rows;
		}
	}

	@Override
	protected void initDefinition(Method method) throws DaoGenerateException {
		definition = new JdbcUpdateDefinition(method);
	}

}
