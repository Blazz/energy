package com.skyline.energy.executor.mongo;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skyline.energy.definition.mongo.MongoRemoveDefinition;
import com.skyline.energy.exception.DaoGenerateException;
import com.skyline.energy.mongo.MongoDataAccessor;
import com.skyline.energy.mongo.MongoShell;

/**
 * Mongo删除操作调用方法
 * 
 * @author wuqh
 * 
 */
public class MongoRemoveExecutor extends AbstractMongoExecutor {
	private static final Logger LOGGER = LoggerFactory.getLogger(MongoRemoveExecutor.class);

	public MongoRemoveExecutor(MongoDataAccessor dataAccessor, Method method) throws DaoGenerateException {
		super(dataAccessor, method);
	}

	@Override
	protected void initDefinition(Method method) throws DaoGenerateException {
		definition = new MongoRemoveDefinition(method);
	}

	@Override
	protected void logShell(String actualShell) {
		LOGGER.info("Mongo删除操作Shell(带Token)[" + actualShell + "]");
	}
	
	@Override
	protected Object doDataAccess(String collectionName, MongoShell shell) {
		return dataAccessor.remove(collectionName, shell);
	}

}
