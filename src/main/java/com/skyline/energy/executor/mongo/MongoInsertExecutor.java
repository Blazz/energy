package com.skyline.energy.executor.mongo;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skyline.energy.definition.mongo.MongoInsertDefinition;
import com.skyline.energy.exception.DaoGenerateException;
import com.skyline.energy.mongo.MongoDataAccessor;
import com.skyline.energy.mongo.MongoShell;

/**
 * Mongo插入操作调用方法
 * 
 * @author wuqh
 * 
 */
public class MongoInsertExecutor extends AbstractMongoExecutor {
	private static final Logger LOGGER = LoggerFactory.getLogger(MongoInsertExecutor.class);

	public MongoInsertExecutor(MongoDataAccessor dataAccessor, Method method) throws DaoGenerateException {
		super(dataAccessor, method);
	}

	@Override
	protected void initDefinition(Method method) throws DaoGenerateException {
		definition = new MongoInsertDefinition(method);
	}

	@Override
	protected void logShell(String actualShell) {
		LOGGER.info("Mongo插入操作Shell(带Token)[" + actualShell + "]");
	}
	
	@Override
	protected Object doDataAccess(String collectionName, MongoShell shell) {
		return dataAccessor.insert(collectionName, shell);
	}
}
