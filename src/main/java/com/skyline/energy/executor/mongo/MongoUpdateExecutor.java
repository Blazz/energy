package com.skyline.energy.executor.mongo;

import java.lang.reflect.Method;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skyline.energy.definition.mongo.MongoUpdateDefinition;
import com.skyline.energy.exception.DaoGenerateException;
import com.skyline.energy.mongo.MongoDataAccessor;
import com.skyline.energy.mongo.MongoShell;
import com.skyline.energy.mongo.MongoUpdate;
import com.skyline.energy.utils.ArgumentUtils;

/**
 * Mongo更新操作调用方法
 * 
 * @author wuqh
 * 
 */
public class MongoUpdateExecutor extends AbstractMongoExecutor {
	private static final Logger LOGGER = LoggerFactory.getLogger(MongoUpdateExecutor.class);

	public MongoUpdateExecutor(MongoDataAccessor dataAccessor, Method method) throws DaoGenerateException {
		super(dataAccessor, method);
	}

	@Override
	protected void initDefinition(Method method) throws DaoGenerateException {
		definition = new MongoUpdateDefinition(method);
	}

	@Override
	public Object execute(Object obj, Object[] args) {
		MongoUpdateDefinition definition = (MongoUpdateDefinition) this.definition;
		
		// 获取Mongo更新中需要用到的查询shell和参数
		Method[] getterMethods = definition.getGetterMethods();
		Integer[] parameterIndexes = definition.getParameterIndexes();

		String actualShell = definition.getShellWithToken();
		List<String> parameterNames = definition.getParsedShell().getParameterNames();
		Object[] paramArray = ArgumentUtils.fetchValues(getterMethods, parameterIndexes, args, parameterNames);

		String collectionName = definition.getCollectionName(args);

		boolean upsert = definition.isUpsert();
		boolean multi = definition.isMulti();

		// 获取Mongo更新中需要用到的更新shell和参数
		Method[] modifierGetterMethods = definition.getModifierGetterMethods();
		Integer[] modifierParameterIndexes = definition.getModifierParameterIndexes();
		String modifier = definition.getModifierShellWithToken();
		List<String> modifierParameterNames = definition.getParsedModifierShell().getParameterNames();
		Object[] modifierParamArray = ArgumentUtils.fetchValues(modifierGetterMethods, modifierParameterIndexes, args,
				modifierParameterNames);

		LOGGER.info("Mongo更新,查询部分Shell(带Token)[" + actualShell + "]");
		LOGGER.info("Mongo更新,更新部分Shell(带Token)[" + modifier + "]");

		// 构造查询、更新的数据结构
		MongoShell query = new MongoShell(actualShell, paramArray);
		MongoUpdate update = new MongoUpdate(modifier, modifierParamArray);
		update.setUpsert(upsert);
		update.setMulti(multi);

		// 执行更新操作
		boolean result = dataAccessor.update(collectionName, query, update);

		return result;
	}

}
