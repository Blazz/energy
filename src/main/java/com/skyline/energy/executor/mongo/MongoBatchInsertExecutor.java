package com.skyline.energy.executor.mongo;

import java.lang.reflect.Method;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skyline.energy.definition.BatchDefinition;
import com.skyline.energy.definition.mongo.MongoBatchInsertDefinition;
import com.skyline.energy.exception.DaoGenerateException;
import com.skyline.energy.mongo.MongoDataAccessor;
import com.skyline.energy.utils.ArgumentUtils;

/**
 * Mongo批量操作调用方法
 * 
 * @author wuqh
 * 
 */
public class MongoBatchInsertExecutor extends AbstractMongoExecutor {
	private static final Logger LOGGER = LoggerFactory.getLogger(MongoBatchInsertExecutor.class);

	public MongoBatchInsertExecutor(MongoDataAccessor dataAccessor, Method method) throws DaoGenerateException {
		super(dataAccessor, method);
	}

	@Override
	protected void initDefinition(Method method) throws DaoGenerateException {
		definition = new MongoBatchInsertDefinition(method);
	}

	@Override
	protected Object getParamArray(Object[] args) {
		return ArgumentUtils.generateBatchQueryArguments(args, (BatchDefinition) definition);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected Object dataAccess(String collectionName, String actualShell, Object paramArray) {
		return dataAccessor.batchInsert(collectionName, actualShell, (List<Object[]>) paramArray);
	}

	@Override
	protected void logShell(String actualShell) {
		LOGGER.info("Mongo批量插入操作Shell(带Token)[" + actualShell + "]");
	}
	
}
