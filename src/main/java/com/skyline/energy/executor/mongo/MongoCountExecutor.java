package com.skyline.energy.executor.mongo;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skyline.energy.definition.mongo.MongoCountDefinition;
import com.skyline.energy.exception.DaoGenerateException;
import com.skyline.energy.mongo.MongoDataAccessor;
import com.skyline.energy.mongo.MongoShell;

/**
 * Mongo统计操作调用方法
 * 
 * @author wuqh
 * 
 */
public class MongoCountExecutor extends AbstractMongoExecutor {
	private static final Logger LOGGER = LoggerFactory.getLogger(MongoCountExecutor.class);

	public MongoCountExecutor(MongoDataAccessor dataAccessor, Method method) throws DaoGenerateException {
		super(dataAccessor, method);
	}

	@Override
	protected void initDefinition(Method method) throws DaoGenerateException {
		definition = new MongoCountDefinition(method);
	}

	@Override
	protected void logShell(String actualShell) {
		LOGGER.info("Mongo统计操作Shell(带Token)[" + actualShell + "]");
	}
	
	@Override
	protected Object doDataAccess(String collectionName, MongoShell shell) {
		long result = dataAccessor.count(collectionName, shell);
		return (new Long(result).intValue());
	}

}
