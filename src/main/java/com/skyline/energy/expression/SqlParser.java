package com.skyline.energy.expression;

/**
 * SQL解析器
 * 
 * @author wuqh
 * 
 */
public class SqlParser extends AbstractExpressionParser {
	private final char[] parameterSeparators = new char[] { '"', '\'', ':', '&', ',', ';', '(', ')', '|', '=', '+',
			'-', '*', '%', '/', '\\', '<', '>', '^' };

	private final String[] startSkip = new String[] { "'", "\"", "--", "/*" };

	private final String[] stopSkip = new String[] { "'", "\"", "\n", "*/" };

	@Override
	protected char[] getParameterSeparators() {
		return parameterSeparators;
	}

	@Override
	protected String[] getStartSkip() {
		return startSkip;
	}

	@Override
	protected String[] getStopSkip() {
		return stopSkip;
	}

	@Override
	protected boolean isNeedIncreaseIndex(char[] expressionChars, int j, char c) {
		return (j < expressionChars.length && expressionChars[j] == ':' && c == ':');
	}

	@Override
	protected int increaseIndex(int i) {
		return (i + 2);
	}

	@Override
	protected boolean isParamStartChar(char c) {
		return (c == ':' || c == '&');
	}

}
