package com.skyline.energy.expression;

/**
 * Mongo的Shell解析器
 * 
 * @author wuqh
 * 
 */
public class MongoShellParser extends AbstractExpressionParser {
	private final char[] parameterSeparators = new char[] { '{', '}', ':', '&', ',', ';', '"', '\'', '[', ']' };

	private final String[] startSkip = new String[] { "/*" };

	private final String[] stopSkip = new String[] { "*/" };

	@Override
	protected char[] getParameterSeparators() {
		return parameterSeparators;
	}

	@Override
	protected String[] getStartSkip() {
		return startSkip;
	}

	@Override
	protected String[] getStopSkip() {
		return stopSkip;
	}

}
